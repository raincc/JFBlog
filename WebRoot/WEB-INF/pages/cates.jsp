<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<c:if test="${!empty article.title}">
			<title>${article.title}</title>
		</c:if>
		<%@include file="/common/common_meta.jsp" %>
		<link href="css/grid.css" rel="stylesheet" type="text/css" />
		<link href="css/icon.css" rel="stylesheet" type="text/css" />
		<link href="css/style.css" rel="stylesheet" type="text/css" />
		<link href="css/custom.css" rel="stylesheet" type="text/css" />
	</head>

	<body>
		<%@include file="/common/header.jsp" %>
		<article class="container content">
			<script type="text/javascript">
			/*728*90*/
			var cpro_id = "u1628768";
			</script>
			<script src="http://cpro.baidustatic.com/cpro/ui/c.js" type="text/javascript"></script>
			<article class="row">
				<section class="article-list col-tb-8 col-mb-12">
					<section id="breadcrumb">
						<a href="">首页</a>
						<span>&gt;</span>${cate_tags}
            		</section>
					<article>
						<ul>
							<c:forEach items="${catetags}" var="t">
								<li>
									<a href="1-${t.id}-0-0.html">${t.name}</a>
								</li>
							</c:forEach>
						</ul>
					</article>
					<%@include file="message_form.jsp" %>
				</section>
				<section class="side-list col-tb-4 col-mb-12">
					<%@include file="latest_blog.jsp" %>
					<%@include file="latest_reply.jsp" %>
					<%@include file="category_tags.jsp" %>
					<%@include file="friendly_links.jsp" %>
				</section>
			</article>
		</article>	
  		<%@include file="/common/footer.jsp" %>
  		<script type="text/javascript">
		    /*120*300 创建于 2014-10-20*/
		var cpro_id = "u2033302";
		</script>
		<script src="http://cpro.baidustatic.com/cpro/ui/f.js" type="text/javascript"></script>
	</body>
</html>