<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<%@include file="/common/common_meta.jsp" %>
<link rel="stylesheet" href="css/zTreeStyle/zTreeStyle.css" type="text/css">
<script src="js/jquery-1.8.0.js" type="text/javascript"></script>
<script type="text/javascript" src="js/jquery.ztree.all-3.5.js"></script>
<script type="text/javascript" src="js/jquery.ztree.xx566.js"></script>
<script type="text/javascript">
	var zNodes = ${admin_tags};
	var userTree;
	$(function(){
		userTree = new UserTree("treeDemo",zNodes,{
			type:"radio",
			frame:"concurrent",
			bind:{"id":"bindid"}
		});
		$.fn.zTree.getZTreeObj("treeDemo").getNodeByParam("id","${tagid}",null).checked = true;
	});
</script>
</head>

<body>
分类：
<div style="width: 250px;height: 530px;overflow: hidden;">
	<div style="overflow: auto; width: 260px;height: 530px;">
		<ul id="treeDemo" class="ztree"></ul>
	</div>
</div>
</body>
</html>