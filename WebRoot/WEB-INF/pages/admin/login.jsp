<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
	<head>
		<%@include file="/common/common_meta.jsp" %>
		<link href="css/normalize.css" rel="stylesheet" type="text/css" />
		<link href="css/grid.css" rel="stylesheet" type="text/css" />
		<link href="css/login.css" rel="stylesheet" type="text/css" />
	</head>

	<body class="body-100">
		<!--[if lt IE 9]>
	        <div class="message error browsehappy">当前网页 <strong>不支持</strong> 你正在使用的浏览器. 为了正常的访问, 请 <a href="http://browsehappy.com/">升级你的浏览器</a>.</div>
	    <![endif]-->
		<div class="typecho-login-wrap">
			<div class="message popup <c:if test='${empty error_info}'>notice</c:if><c:if test='${!empty error_info}'>error</c:if>" id="check_login" style="position: fixed; top: 0px; <c:if test='${!empty error_info}'>display: block;</c:if>"><ul><li>${error_info}</li></ul></div>
		    <div class="typecho-login">
		        <h1><a href="" style="text-decoration:none; color: #ADADAB;">JFBlog</a></h1>
		        <form id="form1" method="post" action="admin/loginToIndex.html">
		            <p>
		                <label class="sr-only" for="name">用户名</label>
		                <input type="text" class="text-l w-100" placeholder="请输入用户名" name="name" id="name" value="${name}">
		            </p>
		            <p>
		                <label class="sr-only" for="password">密码</label>
		                <input type="password" placeholder="请输入密码" class="text-l w-100" name="password" id="password">
		            </p>
		            <input type="hidden" value="" name="returnUrl" id="returnUrl"/>
		            <p class="submit">
		                <button class="btn-l w-100 primary" type="button">登录</button>
		            </p>
<%--		            <p>--%>
<%--		                <label for="remember"><input type="checkbox" id="remember" value="1" class="checkbox" name="remember"> 记住我</label>--%>
<%--		            </p>--%>
		        </form>
		        <p class="more-link">
		        	<a href="">返回首页</a> • <a href="admin/register.html">用户注册</a>
		        </p>
		    </div>
		</div>
		<script src="js/require.js" defer async="true" data-main="js/admin_login"></script>
	</body>
</html>
