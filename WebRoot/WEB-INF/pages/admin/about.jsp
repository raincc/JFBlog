<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
	<%@include file="/common/common_meta.jsp" %>
	<link href="css/grid.css" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" rel="stylesheet" type="text/css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<%@include file="/common/kindeditor.jsp" %>
	<script type="text/javascript">
		var editor1;
		KindEditor.ready(function(K) {
			editor1 = K.create('textarea[id="admin_content"]', {
				uploadJson : '${ctx}/upload/upload.html',
				fileManagerJson : '${ctx}/upload/fileManager.html',
				allowFileManager : true,
				afterCreate : function() {
					var self = this;
					K.ctrl(document, 13, function() {
						if ($("#arttitle").val() == '') {
							alert("您必须设置一个标题");
							return;
						}
						self.sync();
						document.forms['about'].submit();
					});
					K.ctrl(self.edit.doc, 13, function() {
						if ($("#arttitle").val() == '') {
							alert("您必须设置一个标题");
							return;
						}
						self.sync();
						document.forms['about'].submit();
					});
				}
			});
			prettyPrint();
		});
	</script>
</head>

<body>
	<%@include file="../include/header.jsp" %>
	<section>
		<div class="main">
			<div class="body container">
				<div class="colgroup">
			    	<div class="typecho-page-title col-mb-12">
						<form action="admin/aboutPublish" method="post" id="form1" name="about" onsubmit="javascript:editor1.sync();">
							<input type="hidden" name="article.id" value="-1"/>
							<input type="hidden" name="article.userid" value="-1"/>
							<input type="hidden" id="bindid" name="article.tagsid" value="-1"/>
							文章标题：<input type="text" style="width: 35%;" name="article.title" id="arttitle" value="${realfighter.title}" placeholder="请输入文章标题"/>
							</br>
							<textarea cols="100" rows="20" name="article.content" id="admin_content">${realfighter.content}</textarea>
							<input type="button" value="保存" id="admin_about"/>(提交快捷键: Ctrl + Enter)
						</form>
			    	</div>
				</div>
		   	</div>
		</div>
	</section>
	<script src="js/require.js" defer async="true" data-main="js/admin_about"></script>
</body>
</html>