<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
	<%@include file="/common/common_meta.jsp" %>
	<link href="css/grid.css" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" rel="stylesheet" type="text/css" />
	<link href="css/normalize.css" rel="stylesheet" type="text/css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link href="css/dialog.css" rel="stylesheet" type="text/css"/>
</head>
<body>
	<%@include file="../include/header.jsp" %>
	<section>
		<div class="main">
			<div class="body container">
				<c:if test="${isadmin}">
					<div>
						<span class="icon-archive"></span><a id="arts_indexes" title="创建索引" style="cursor: pointer;">创建索引 </a>
					</div> 
				</c:if>
				<div class="colgroup">
			    	<div class="typecho-page-title col-mb-12">
				        <table width="100%">
		                    <colgroup>
		                        <col width="40%">
		                        <col width="10%">
		                        <col width="10%">
		                        <col width="15%">
		                        <col width="10%">
		                        <col width="15%">
		                    </colgroup>
		                    <thead>
		                        <tr>
		                            <th>标题</th>
		                            <th>标签</th>
		                            <th>关注</th>
		                            <th>日期</th>
		                            <th>状态</th>
		                            <th>操作</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    <c:forEach items="${page.list}" var="blogs">
		                    	<tr>
		                        	<td>
		                        		<jf:check type="${blogs.type}" code="1"><img src="image/ico_Original.gif"></jf:check>
										<jf:check type="${blogs.type}" code="2"><img src="image/ico_Repost.gif"></jf:check>
										<jf:check type="${blogs.type}" code="4"><img src="image/ico_Translated.gif"></jf:check>
										<jf:check type="${blogs.type}" code="8"><img src="image/ico_Recommend.gif"></jf:check>
										<jf:check type="${blogs.type}" code="16"><img src="image/ico_Stick.gif"></jf:check>
		                        		<a href="detail/${blogs.id}.html" target="_blank">${blogs.title}</a>
		                        	</td>
		                        	<td align="center"><a href="1-${blogs.tagsid}-0-0.html" target="_blank">${blogs.name}</a></td>
		                        	<td align="center">${blogs.clicks}</td>
		                        	<td align="center"><fmt:formatDate value="${blogs.createtime}" pattern="yyyy/MM/dd HH:mm" /></td>
		                        	<td align="center">
		                        		<c:if test="${blogs.isdraft==0}">正常</c:if>
		                        		<c:if test="${blogs.isdraft==1}">草稿</c:if>
		                        	</td>
		                        	<td align="center"><a href="admin/toPublish/${blogs.id}.html" style="cursor: pointer;">编辑</a>|<a style="cursor: pointer;" ref="admin/artdel/${blogs.id}.html" id="art_delete">删除</a></td>
		                        </tr>
		                    </c:forEach>
		                    <c:if test="${empty page.list}">
		                    	<tr>
		                        	<td colspan="6"><h4 align="center">没有任何文章</h4></td>
		                        </tr>
							</c:if>
							<c:if test="${!empty page.list}">
								<tr align="right">
									<td colspan="6">
										<c:if test="${page.num!=1}">
											<a href="admin/artList/1-0-0-0.html">首页</a>
											<a href="admin/artList/${page.num-1}-0-0-0.html">上一页</a>
										</c:if>
										<c:forEach var="i" begin="${page.begin}" end="${page.end}">
											<a <c:if test="${page.num==i}">style="font-size: 18px;"</c:if> href="admin/artList/${i}-0-0-0.html">${i}</a>
										</c:forEach>
										<c:if test="${page.navCount!=0&&page.num!=page.last}">
											<a href="admin/artList/${page.num+1}-0-0-0.html">下一页</a>
											<a href="admin/artList/${page.navCount}-0-0-0.html">尾页</a>
										</c:if>
									</td>
								</tr>
							</c:if>
		                    </tbody>
		                </table>
			    	</div>
				</div>
		   	</div>
		</div>
	</section>
	<script src="js/require.js" defer async="true" data-main="js/admin_article"></script>
</body>
</html>