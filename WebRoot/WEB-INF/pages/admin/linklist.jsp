<%@ page contentType="text/html; charset=UTF-8"%>
<%@include file="/common/common_taglib.jsp" %>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xml:lang="zh-CN" xmlns="http://www.w3.org/1999/xhtml" lang="zh-CN">
<head>
	<%@include file="/common/common_meta.jsp" %>
	<link href="css/grid.css" rel="stylesheet" type="text/css" />
	<link href="css/icon.css" rel="stylesheet" type="text/css" />
	<link href="css/normalize.css" rel="stylesheet" type="text/css" />
	<link href="css/style.css" rel="stylesheet" type="text/css" />
	<link href="css/dialog.css" rel="stylesheet" type="text/css"/>
</head>
<body>
	<%@include file="../include/header.jsp" %>
	<section>
		<div class="main">
			<div class="body container">
				<div>
					<span class="icon-archive"></span><a linksid="0" id="links_update" title="新增友链" style="cursor: pointer;">新增友链 </a>
				</div> 
				<div class="colgroup">
			    	<div class="typecho-page-title col-mb-12">
				        <table width="100%">
		                    <colgroup>
		                        <col width="15%">
		                        <col width="30%">
		                        <col width="10%">
		                        <col width="20%">
		                        <col width="10%">
		                        <col width="15%">
		                    </colgroup>
		                    <thead>
		                        <tr>
		                            <th>名称</th>
		                            <th>链接</th>
		                            <th>点击</th>
		                            <th>日期</th>
		                            <th>排序</th>
		                            <th>操作</th>
		                        </tr>
		                    </thead>
		                    <tbody>
		                    <c:forEach items="${page.list}" var="links">
		                    	<tr>
		                        	<td><a href="click/${links.id}.html" target="_blank">${links.name}</a></td>
		                        	<td align="center"><a href="click/${links.id}.html" target="_blank">${links.url}</a></td>
		                        	<td align="center">${links.counts}</td>
		                        	<td align="center"><fmt:formatDate value="${links.createtime}" pattern="yyyy/MM/dd HH:mm" /></td>
		                        	<td align="center">${links.orders}</td>
		                        	<td align="center"><a linksid="${links.id}" id="links_update" style="cursor: pointer;">编辑</a>|<a linksid="${links.id}" id="links_delete" style="cursor: pointer;">删除</a></td>
		                        </tr>
		                    </c:forEach>
		                    <c:if test="${empty page.list}">
		                    	<tr>
		                        	<td colspan="6"><h4 align="center">没有添加友情链接</h4></td>
		                        </tr>
							</c:if>
							<c:if test="${!empty page.list}">
								<tr align="right">
									<td colspan="6">
										<c:if test="${page.num!=1}">
											<a href="admin/linkList/1.html">首页</a>
											<a href="admin/linkList/${page.num-1}.html">上一页</a>
										</c:if>
										<c:forEach var="i" begin="${page.begin}" end="${page.end}">
											<a <c:if test="${page.num==i}">style="font-size: 18px;"</c:if> href="admin/linkList/${i}.html">${i}</a>
										</c:forEach>
										<c:if test="${page.navCount!=0&&page.num!=page.last}">
											<a href="admin/linkList/${page.num+1}.html">下一页</a>
											<a href="admin/linkList/${page.navCount}.html">尾页</a>
										</c:if>
									</td>
								</tr>
							</c:if>
		                    </tbody>
		                </table>
			    	</div>
				</div>
		   	</div>
		</div>
	</section>
	<script src="js/require.js" defer async="true" data-main="js/admin_links"></script>
</body>
</html>