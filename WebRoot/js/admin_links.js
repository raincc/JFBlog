//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"jquery.artDialog" : "js/jquery.artDialog",
		"admin.links" : "xx566/admin.links"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		}
	}
});
//友链管理
require( [ "jquery", "admin.links" ], function($, links) {
	$("a[id^='links_update']").click(links.update);
	$("a[id^='links_delete']").click(links.remove);
});