//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"jquery.md5" : "js/jquery.md5",
		"xx566.index.message" : "xx566/index.message",
		"jquery.artDialog" : "js/jquery.artDialog",
		"xx566.index" : "xx566/index"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		}
	}
});
//留言功能
require( [ "jquery", "xx566.index.message" ], function($, message) {
	$("#msgpid").val(0);
	//发布留言
	$("#msg_submit").live("click", message.addMsg);
	//跟随留言
	$("a[rel^='follow']").live("click", message.followMsg);
	//取消跟随
	$("a[id^='cancel-comment-reply-link']").live("click", message.cancleMsg);
});
//公共部分
require( [ "jquery", "xx566.index" ], function($, index) {
	//站内搜索
	$("#search_btn").click(index.search);
	//导航分类点击
	$("a[id^='index_cate']").click(index.category);
	//微信二维码
	$("#xx566_qrcode").bind(index.qrcode);
});