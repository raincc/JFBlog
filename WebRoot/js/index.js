//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"jquery.artDialog" : "js/jquery.artDialog",
		"xx566.index" : "xx566/index"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		}
	}
});
//首页搜索
require( [ "jquery", "xx566.index" ], function($, index) {
	//站内搜索
	$("#search_btn").click(index.search);
	//导航分类点击
	$("a[id^='index_cate']").click(index.category);
	//微信二维码
	$("#xx566_qrcode").bind(index.qrcode);
});