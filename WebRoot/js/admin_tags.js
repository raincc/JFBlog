//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"jquery.artDialog" : "js/jquery.artDialog",
		"admin.tags" : "xx566/admin.tags"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		}
	}
});
//标签管理
require( [ "jquery", "admin.tags" ], function($, tags) {
	$("#tags_btn").click(tags.add);
	$("a[id^='tags_update']").click(tags.update);
	$("a[id^='tags_delete']").click(tags.remove);
});