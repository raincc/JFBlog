(function($, window) {
	var view = {
		dbClickExpand : false,
		showLine : true,
		showTitle : false,
		selectedMulti : false,
		expandSpeed : ($.browser.msie && parseInt($.browser.version) <= 6) ? "" : "fast"
	},data = {
		simpleData : {
			enable : true,
			idKey : "id",
			pIdKey : "pId"
		}
	},radio = {
		enable : true,
		chkStyle : "radio",
		radioType : "all"
	},check = {
		enable : true
	},treeObj,bind,frame;
	var extend = $.extend;
	//绑定点击事件
	function bindOnCheck(event, treeId, treeNode){
		var resultArr;
		for(var key in bind){
			resultArr = new Array();
			for(var i =0;i<treeObj.getCheckedNodes(true).length;i++){
				if(!treeObj.getCheckedNodes(true)[i].isParent){
					for(var key1 in treeObj.getCheckedNodes(true)[i]){
						if(key==key1){
							resultArr.push(treeObj.getCheckedNodes(true)[i][key1]);
							break;
						}
					}
				}
			/*	if(treeObj.getCheckedNodes(true)[i].isParent&&treeObj.getCheckedNodes(true)[i].level==0){
					for(var key1 in treeObj.getCheckedNodes(true)[i]){
						if(key==key1){
							resultArr.push(treeObj.getCheckedNodes(true)[i][key1]);
					//		alert(treeObj.getCheckedNodes(true)[i].children);
							break;
						}
					}
				}*/
			}
			if(frame == "contain"){//包含结构
				parent.document.getElementById(bind[key]).value = resultArr;
			}else if(frame == "concurrent"){//并列结构
				parent.rightFrame.document.getElementById(bind[key]).value = resultArr;
			}else{//默认结构
				document.getElementById(bind[key]).value = resultArr;
			}
		}
	}
	//初始化树形
	function initTree(id, setting, zNodes){
		this.treeObj = $.fn.zTree.init($("#"+id), setting, zNodes);
		this.setting = this.treeObj.setting;
		this.zNodes = zNodes;
		return this;
	}
	//获取选中的节点
	function getCheckedValue(obj){
		if (obj.treeObj == null) {
			return [];
		}
		var nodes = obj.treeObj.getCheckedNodes();
		return getValues(obj.treeObj, nodes);
	}
	//获取节点对象
	function getValues(treeObj, nodes){
		var temp = $.grep(nodes, function(node) {
			if (!node.isParent)
				return true;
		});
		var ret = treeObj.transformToArray(temp) || [];
		for ( var i = 0; i < ret.length; i++) {
			ret[i] = extend( {}, ret[i]);
		}
		return ret;
	}
	function UserTree(id, zNodes, jsonObj){
		this.constructor = UserTree;
		var setting;
		if(jsonObj!=null){
			if(jsonObj.bind!=null&&jsonObj.bind!=""){
				bind = jsonObj.bind;
			}
			if(jsonObj.frame!=null&&jsonObj.frame!=""){
				frame = jsonObj.frame;
			}
			if(jsonObj.type=="radio"){//单选的树
				setting = {
					view : view,
					check : radio,
					data : data,
					callback : {
						onCheck : bindOnCheck
					}
				};
			}else if(jsonObj.type=="check"){//复选的树
				setting = {
					view : view,
					check : check,
					data : data,
					callback : {
						onCheck : bindOnCheck
					}
				};
			}else{//普通的树形
				setting = {
					view : view,
					data : data
				};
			}
		}else{//默认为普通树形
			setting = {
				view : view,
				data : data
			};
		}
		var obj = initTree(id, setting, zNodes);
		treeObj = obj.treeObj;
		return obj;
	}
	window.getCheckedValue = getCheckedValue;
	window.UserTree = UserTree;
})(jQuery, window);
