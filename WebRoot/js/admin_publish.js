//js配置
require.config( {
	baseUrl : "",
	paths : {
		"jquery" : "js/jquery-1.8.0",
		"fileupload" : "js/ajaxfileupload",
		"jquery.artDialog" : "js/jquery.artDialog",
		"admin.publish" : "xx566/admin.publish"
	},
	shim : {
		"jquery.artDialog" : {
			deps : [ "jquery" ],
			exports : "artDialog"
		},
		"fileupload" : {
			deps : [ "jquery" ],
			exports : "fileupload"
		}
	}
});
// 文章管理
require( [ "jquery", "admin.publish", "fileupload", "jquery.artDialog" ],
		function($, article) {
	// 配图上传
	$("#fileUpload").click(article.fileUpload);
	// 选择历史
	$("#choose_history").click(article.chooseHistory);
	// 图片上传
	$("img[id^='history_img']").click(article.clickHistory);
	// 文章保存
	$("#admin_publish").click(article.saveArticle);
});