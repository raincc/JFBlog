define( [ "jquery" ], function($) {
	var article = {};
	article.fileUpload = function() {
		$.ajaxFileUpload( {
			url : 'upload/fileUpload.html', // 用于文件上传的服务器端请求地址
			secureuri : false, // 是否需要安全协议，一般设置为false
			fileElementId : 'artpic', // 文件上传域的ID
			dataType : 'json', // 返回值类型 一般设置为json
			success : function(data, status) // 服务器成功响应处理函数
			{
				data = data.replace(/<\/?[^>]*>/g, ''); // 去除HTML tag
				data = data.replace(/[ | ]*\n/g, '\n'); // 去除行尾空白
				$("#admin_img").attr("src", data);
				$("#art_hidden").val(data);
			},
			error : function(ret, status, e)// 服务器响应失败处理函数
			{
				alert(e);
			}
		})
	}
	article.chooseHistory = function(){
		$.dialog( {
			lock : true,
			background : '#000000',
			opacity : 0.5,
			fixed : true,
			drag : false,
			resize : false,
			cancel : false,
			title : '选择历史',
			content : document.getElementById("history_dialog"),
			button : [ {
				name : '取消'
			} ]
		});
	}
	article.clickHistory = function(){
		$("#admin_img").attr("src", $(this).attr("src"));
		$("#art_hidden").val($(this).attr("src"));
		var list = $.dialog.list;
		for (var i in list) {
		    list[i].close();
		};
	}
	article.saveArticle = function(){
		if ($("#art_hidden").val() == '') {
			alert("请上传文章配图");
			return;
		}
		if ($("#bindid").val() == '') {
			alert("您必须选择一个分类");
			return;
		}
		if ($("#arttitle").val() == '') {
			alert("您必须设置一个标题");
			return;
		}
		//计算文章类型值
		var artvalue = 0;
		artvalue += parseInt($("#articletype").val());
		if($("#Recommend").attr("checked")){
			artvalue += 8;
		}
		if($("#Stick").attr("checked")){
			artvalue += 16;
		}
		$("#artvalue").val(artvalue);
		$("#form1").attr("target", "_parent");
		$("#form1").submit();
	}
	return article;
});