define( [ "jquery", "jquery.md5" ], function($) {
	var login = {};
	login.submit = function() {
		var name = $("#form1 #name").val();
		var pwd = $("#form1 #password").val();
		if (name == '' || pwd == '') {
			$("#check_login li").html("用户名或密码不能为空");
			$("#check_login").fadeIn('slow');
			$("#check_login").removeClass("error").removeClass("notice")
					.addClass("notice");
			setTimeout(function() {
				$("#check_login").fadeOut('slow');
			}, 5000);
			return;
		}
		$("#form1 #password").val($.md5(pwd));
		$("#returnUrl").val(window.location.href);
		$("#form1").submit();
	}
	return login;
});