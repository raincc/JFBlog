define( [ "jquery", "jquery.artDialog" ], function($) {
	var article = {};
	//创建索引
	article.createIndexes = function() {
		$.dialog( {
			lock : true,
			background : '#000000',
			opacity : 0.5,
			fixed : true,
			drag : false,
			resize : false,
			title : '消息提示',
			content : '此操作会重置所有索引文件，确认继续吗？',
			button : [ {
				name : '确定',
				callback : function() {
					$.ajax( {
						type : "POST",
						url : "admin/generateIndexes.html",
						success : function(msg) {
							alert(msg);
						}
					});
				}
			}, {
				name : '取消'
			} ]
		});
	}
	//删除文章
	article.deleteArticle = function() {
		var href = $(this).attr("ref");
		$.dialog( {
			lock : true,
			background : '#000000',
			opacity : 0.5,
			fixed : true,
			drag : false,
			resize : false,
			title : '消息提示',
			content : '确定删除此文章吗？',
			button : [ {
				name : '确定',
				callback : function() {
					$.ajax( {
						type : "POST",
						url : href,
						success : function(msg) {
							alert(msg);
							window.location.href = window.location.href;
						}
					});
				}
			}, {
				name : '取消'
			} ]
		});
	}
	return article;
});