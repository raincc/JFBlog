define( [ "jquery", "jquery.md5" ], function($) {
	var reg = {};
	reg.submit = function() {
		var name = $("#form1 #name").val();
		var pwd = $("#form1 #password").val();
		var repwd = $("#form1 #repassword").val();
		var error_info = false;
		if (name == '' || pwd == '' || repwd == '') {
			error_info = true;
			errors = "用户名或密码不能为空";
		} else if (pwd != repwd) {
			error_info = true;
			errors = "两次密码输入不一致";
		}
		if (error_info) {
			$("#check_reg li").html(errors);
			$("#check_reg").removeClass("error").removeClass("notice")
					.addClass("notice");
			$("#check_reg").fadeIn('slow');
			setTimeout(function() {
				$("#check_reg").fadeOut('slow');
			}, 5000);
			return;
		}
		$("#form1 #password").val($.md5(pwd));
		$("#form1 #repassword").val($.md5(repwd));
		$("#form1").submit();
	}
	return reg;
});