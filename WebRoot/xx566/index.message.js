define( [ "jquery", "jquery.md5" ], function($) {
	var message = {};
	$("#msgpid").val(0);
	// 发布留言
	message.addMsg = function() {
		$("#response_message span[id]").html("");
		// $("#msg_form span").html("");
		var msgForm = $(this).parent().parent();
		$(msgForm).find("#returnUrl").val(window.location.href);
		if (xx566_userid == '') {
			var name = $(msgForm).find("#username").val();
			var pwd = $(msgForm).find("#userpwd").val();
			var msg = $(msgForm).find("#usermsg").val();
			if (name == '' || pwd == '') {
				$(msgForm).find("#nameMsg").html("请输入用户名或密码");
				return;
			}
			if (msg == '') {
				$(msgForm).find("#nameMsg").html("请输入留言内容");
				return;
			}
			$.post("isNameCorrect.html", {
				name : name,
				pwd : $.md5(pwd)
			}, function(data) {
				if (data != '') {
					$(msgForm).find("#nameMsg").html(data);
					return;
				} 
				$(msgForm).find("#userpwd").val($.md5(pwd));
				$(msgForm).submit();
			});
		} else {
			$(msgForm).submit();
		}
	}
	// 留言form跟随
	message.followMsg = function() {
		$("article[rel^='msg_reback']").hide();
		$("#response_message").hide();
		$("#response_message  div:hidden").show();
		$("#response_message span[id]").html("");
		var hml = $("#response_message").html();
		$(this).parent().after(hml);
		$(this).parent().next().show();
		$(this).parent().next().find("#msgpid").val($(this).attr("msgpid"));
	}

	// 留言form取消跟随
	message.cancleMsg = function() {
		$("#response_message article[rel^='msg_reback']").show();
		$("#response_message").show();
		$("#response_message  .cancel-comment").hide();
		$(this).parent().parent().remove();
	}
	return message;
});