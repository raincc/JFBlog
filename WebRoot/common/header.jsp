<%@ page language="java" import="java.util.*" pageEncoding="UTF-8"%>
<!--[if lt IE 9]>
    <div class="browsehappy">当前网页 <strong>不支持</strong> 你正在使用的浏览器. 为了正常的访问, 请 <a href="http://browsehappy.com/">升级你的浏览器</a>.</div>
<![endif]-->
<div class="icon-rocket" id="back-to-top"></div>
<header class="navbar"> <section class="container clearfix">
	<h2 class="navbar-header">
		<a href="about/n1.html" class="nav-brand">JFBlog</a>
	</h2>
	<div id="mobile-menu" class="icon-menu"></div>
	<nav class="navbar-inner clearfix">
	<ul class="navbar-nav">
		<li>
			<a href="" ref="navbar_click" title="首页"> <span class="icon-home"></span> 首页 </a>
		</li>
		<li class="navigator">
			<a title="导航"> <span class="icon-menu"></span> 导航 </a>
			<article class="dropdown">
			<div class="dropdown-arrow1"></div>
			<div class="dropdown-arrow2"></div>
			<div class="submenu">
				<div class="tab-content">
					<table>
						<tbody>
							<tr>
								<td class="tdleft">
									归档
								</td>
								<td class="tdright">
									<c:forEach items="${archive_times}" var="at">
									    <a href="1-0-${at.createtime}.html">
									    ${fn:split(at.createtime,'-')[0]}年${fn:split(at.createtime,'-')[1]}月
									    </a>
							    	</c:forEach>
								</td>
							</tr>
							<tr>
								<td class="tdleft">
									分类
								</td>
								<td class="tdright">
									<c:forEach items="${tags_cateList}" var="cat">
								    	<a ref="${cat.cate}.html" id="index_cate">${cat.cate}</a>
							    	</c:forEach>
								</td>
							</tr>
							<tr>
								<td class="tdleft">
									其它
								</td>
								<td class="tdright">
									<c:if test="${sessionScope.isadmin}">
										<a href="admin/artList.html">进入后台</a>
									</c:if>
									<a href="http://git.oschina.net/realfighter/JFBlog/" target="_blank">JFBlog源码</a>
									<a href="https://git.oschina.net/realfighter/JFBlog-maven/" target="_blank">JFBlog-maven源码</a>
									<a href="http://git.oschina.net/realfighter/workapp/" target="_blank">Workapp源码</a>
								</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
			</article>
		</li>
		<li>
			<a href="archive/n3.html" title="归档"> <span class="icon-archive"><span> 归档 </span> </span> </a>
		</li>
		<li>
			<a href="message/n2.html" title="留言"> <span class="icon-guestbook"><span> 留言 </span> </span> </a>
		</li>
		<li>
			<a href="about/n1.html" title="关于JFBlog"> <span class="icon-about"><span> 关于JFBlog </span> </span> </a>
		</li>
	</ul>
	</nav>
	<c:if test="${sessionScope.xx566_userid==null}">
		<a class="touxiang" title="登录" style="cursor: pointer;" href='admin/login.html'>
			<span><img alt="头像" src="image/5.jpg">登录</span>
		</a>
	</c:if>
	<c:if test="${sessionScope.xx566_userid!=null}">
		<a class="touxiang" title="登出（${sessionScope.xx566_username}）" style="cursor: pointer;" href='admin/logout.html'>
			<span><img alt="头像" src="${sessionScope.xx566_userpic}">登出（${sessionScope.xx566_username}）</span>
		</a>
	</c:if>
	<form class="navbar-search" method="post" id="search_form">
		<input placeholder="站内搜索" class="search-input" type="text" id="search_key" style="color:#6d6d6d;" onfocus="javascript:if($(this).val()=='站内搜索'){$(this).val('')}" onblur="javascript:if($(this).val()==''){$(this).val('站内搜索')}" value="<c:if test="${empty search_key}">站内搜索</c:if><c:if test="${!empty search_key}">${search_key}</c:if>">
		<button type="button" class="search-submit icon-search" id="search_btn"></button>
	</form>
	</section>
</header>