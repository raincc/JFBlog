package com.jfblog.controller;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Hashtable;
import java.util.List;
import java.util.Map;
import java.util.Random;

import com.google.common.base.Splitter;
import com.google.common.collect.Lists;
import com.google.common.io.Files;
import com.jfinal.core.Controller;
import com.jfinal.core.JFinal;
import com.jfinal.kit.PathKit;
import com.jfinal.upload.UploadFile;

@SuppressWarnings("unchecked")
public class FileUploadController extends Controller {

	/**
	 * Tinymce文件上传
	 * 
	 * @Title: index
	 * @author Realfighter void
	 * @throws
	 */
	public void index() {
		String saveDirectory = PathKit.getWebRootPath() + File.separator + "upload";
		UploadFile uploadFile = getFile("userfile", saveDirectory, 10 * 1024 * 1024, "UTF-8");
		// System.out.println(uploadFile.getFile().getAbsolutePath());
		renderHtml("<script>window.parent." + getPara("editor") + ".insertContent('<img src=\"upload/"
				+ uploadFile.getFileName() + "\"/>');window.parent." + getPara("editor")
				+ ".plugins.upload.finish();</script>");
	}

	/**
	 * 文章配图上传
	 * 
	 * @Title: fileUpload
	 * @author Realfighter void
	 * @throws
	 */
	public void fileUpload() {
		// 配图保存在upload/index目录
		String saveDirectory = PathKit.getWebRootPath() + File.separator + "upload" + File.separator + "index";
		UploadFile uploadFile = getFile("artpic", saveDirectory, 10 * 1024 * 1024, "UTF-8");
		String src = "upload/index/" + uploadFile.getFileName();
		renderText(src);
	}

	/**
	 * KindEditor文件上传
	 * 
	 * @throws Exception
	 */
	public void upload() throws Exception {
		// 定义允许上传的文件扩展名
		HashMap<String, String> extMap = new HashMap<String, String>() {
			private static final long serialVersionUID = 1L;
			{
				put("image", "gif,jpg,jpeg,png,bmp");
				put("flash", "swf,flv");
				put("media", "swf,flv,mp3,wav,wma,wmv,mid,avi,mpg,asf,rm,rmvb");
				put("file", "doc,docx,xls,xlsx,ppt,htm,html,txt,zip,rar,gz,bz2");
			}
		};

		Map result = new HashMap();
		String dirName = getPara("dir") == null ? "image" : getPara("dir");
		String realpath = PathKit.getWebRootPath() + "/upload";
		UploadFile uf = getFile("imgFile", realpath);
		String affix_id = "";
		String affix_name = "";
		if (uf != null) {
			affix_name = uf.getFile().getName();
			File file = uf.getFile();
			// 检查扩展名
			String fileExt = affix_name.substring(affix_name.lastIndexOf(".") + 1).toLowerCase();
			if (!Lists.newArrayList(Splitter.on(",").split(extMap.get(dirName))).contains(fileExt)) {
				result.put("error", 1);
				result.put("message", "上传文件扩展名是不允许的扩展名。\n只允许" + extMap.get(dirName) + "格式。");
				file.delete();
			} else {
				// yyyyMMddHHmmss
				String dateStr = String.format("%1$tY%<tm%<td%<tH%<tM%<tS", Calendar.getInstance());
				affix_id = dateStr + "_" + new Random().nextInt(1000) + "." + fileExt;
				File savefile = new File(new File(realpath), affix_id);
				Files.copy(file, savefile);
				if (file.exists()) {
					file.delete();
				}
				result.put("error", 0);
				result.put("url", JFinal.me().getContextPath() + "/upload/" + affix_id);
			}
		} else {
			result.put("error", 1);
			result.put("message", "请选择文件");
		}

		renderJson(result);
	}

	/**
	 * 文件管理
	 */
	public void fileManager() {
		String[] fileTypes = new String[] { "gif", "jpg", "jpeg", "png", "bmp" };
		String currentPath = PathKit.getWebRootPath() + "/upload/";
		File currentPathFile = new File(currentPath);
		final List<Hashtable> fileList = new ArrayList<Hashtable>();
		if (currentPathFile.listFiles() != null) {
			for (File file : currentPathFile.listFiles()) {
				Hashtable<String, Object> hash = new Hashtable<String, Object>();
				String fileName = file.getName();
				if (file.isDirectory()) {
					hash.put("is_dir", true);
					hash.put("has_file", (file.listFiles() != null));
					hash.put("filesize", 0L);
					hash.put("is_photo", false);
					hash.put("filetype", "");
				} else if (file.isFile()) {
					String fileExt = fileName.substring(fileName.lastIndexOf(".") + 1).toLowerCase();
					hash.put("is_dir", false);
					hash.put("has_file", false);
					hash.put("filesize", file.length());
					hash.put("is_photo", Arrays.<String> asList(fileTypes).contains(fileExt));
					hash.put("filetype", fileExt);
				}
				hash.put("filename", fileName);
				hash.put("datetime", new SimpleDateFormat("yyyy-MM-dd HH:mm:ss").format(file.lastModified()));
				fileList.add(hash);
			}
		}
		// 排序形式，name or size or type
		String order = getPara("order") != null ? getPara("order").toLowerCase() : "name";
		if ("size".equals(order)) {
			Collections.sort(fileList, new SizeComparator());
		} else if ("type".equals(order)) {
			Collections.sort(fileList, new TypeComparator());
		} else {
			Collections.sort(fileList, new NameComparator());
		}

		Map<String, Object> result = new HashMap<String, Object>() {
			private static final long serialVersionUID = 1L;
			{
				put("moveup_dir_path", "");
				put("current_dir_path", "");
				put("current_url", JFinal.me().getContextPath() + "/upload/");
				put("total_count", fileList.size());
				put("file_list", fileList);
			}
		};
		renderJson(result);
	}

	public class NameComparator implements Comparator {
		public int compare(Object a, Object b) {
			Hashtable hashA = (Hashtable) a;
			Hashtable hashB = (Hashtable) b;
			if (((Boolean) hashA.get("is_dir")) && !((Boolean) hashB.get("is_dir"))) {
				return -1;
			} else if (!((Boolean) hashA.get("is_dir")) && ((Boolean) hashB.get("is_dir"))) {
				return 1;
			} else {
				return ((String) hashA.get("filename")).compareTo((String) hashB.get("filename"));
			}
		}
	}

	public class SizeComparator implements Comparator {
		public int compare(Object a, Object b) {
			Hashtable hashA = (Hashtable) a;
			Hashtable hashB = (Hashtable) b;
			if (((Boolean) hashA.get("is_dir")) && !((Boolean) hashB.get("is_dir"))) {
				return -1;
			} else if (!((Boolean) hashA.get("is_dir")) && ((Boolean) hashB.get("is_dir"))) {
				return 1;
			} else {
				if (((Long) hashA.get("filesize")) > ((Long) hashB.get("filesize"))) {
					return 1;
				} else if (((Long) hashA.get("filesize")) < ((Long) hashB.get("filesize"))) {
					return -1;
				} else {
					return 0;
				}
			}
		}
	}

	public class TypeComparator implements Comparator {
		public int compare(Object a, Object b) {
			Hashtable hashA = (Hashtable) a;
			Hashtable hashB = (Hashtable) b;
			if (((Boolean) hashA.get("is_dir")) && !((Boolean) hashB.get("is_dir"))) {
				return -1;
			} else if (!((Boolean) hashA.get("is_dir")) && ((Boolean) hashB.get("is_dir"))) {
				return 1;
			} else {
				return ((String) hashA.get("filetype")).compareTo((String) hashB.get("filetype"));
			}
		}
	}

}
