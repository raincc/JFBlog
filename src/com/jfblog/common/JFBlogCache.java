package com.jfblog.common;

import java.util.List;

import com.jfblog.bean.CacheBean;
import com.jfblog.model.Article;
import com.jfblog.model.Friendly;
import com.jfblog.model.Message;
import com.jfblog.model.Tags;
import com.jfinal.core.Controller;
import com.jfinal.plugin.activerecord.Page;
import com.jfinal.plugin.ehcache.CacheKit;
import com.jfinal.plugin.ehcache.IDataLoader;

public class JFBlogCache {

	public static enum CacheKey {
		tags, friendlys, articles, messages, archives, cates
	}

	public static IDataLoader getIDataLoaderByKey(final CacheKey cacheKey) {
		final IDataLoader iDataLoader = new IDataLoader() {

			public Object load() {
				switch (cacheKey) {
				case tags:
					return Tags.dao.findAll();
				case messages:
					return Message.dao.findMsg4Page(1, 10);
				case cates:
					return Tags.dao.findCateList();
				case friendlys:
					return Friendly.dao.findAll();
				case archives:
					return Article.dao.findAllArchiveTimes();
				case articles:
					return Article.dao.findArticles4Page(1, 10, 0, "0-0", 0, false);
				}
				return null;
			}

		};
		return iDataLoader;
	}

	/**
	 * 设置需要缓存的信息
	 * 
	 * @param controller
	 */
	public static void setAllCache(Controller controller) {
		setTagsCache(controller);
		setMessagesCache(controller);
		setCatesCache(controller);
		setFriendlysCache(controller);
		setArchiveTimesCache(controller);
		setArticlesCache(controller);
	}

	/**
	 * 首页文章缓存
	 * 
	 * @Title: setArticlesCache
	 * @author Realfighter
	 * @param controller
	 *            void
	 * @throws
	 */
	private static void setArticlesCache(Controller controller) {
		Page<Article> articles = CacheKit.get(CacheBean.ARTICLES, CacheBean.ARTICLES, JFBlogCache
				.getIDataLoaderByKey(CacheKey.articles));
		controller.setAttr("articles", articles);
	}

	/**
	 * 归档文章缓存
	 * 
	 * @Title: setArchiveTimesCache
	 * @author Realfighter
	 * @param controller
	 *            void
	 * @throws
	 */
	private static void setArchiveTimesCache(Controller controller) {
		List<Article> archive_times = CacheKit.get(CacheBean.ARCHIVE_TIMES, CacheBean.ARCHIVE_TIMES, JFBlogCache
				.getIDataLoaderByKey(CacheKey.archives));
		controller.setAttr("archive_times", archive_times);
	}

	/**
	 * 友链缓存
	 * 
	 * @Title: setFriendlysCache
	 * @author Realfighter
	 * @param controller
	 *            void
	 * @throws
	 */
	private static void setFriendlysCache(Controller controller) {
		List<Friendly> friendlys = CacheKit.get(CacheBean.FRIENDLYS, CacheBean.FRIENDLYS, JFBlogCache
				.getIDataLoaderByKey(CacheKey.friendlys));
		controller.setAttr("friendlys", friendlys);
	}

	/**
	 * 分类缓存
	 */
	private static void setCatesCache(Controller controller) {
		List<Tags> cateList = CacheKit.get(CacheBean.CATES, CacheBean.CATES, JFBlogCache
				.getIDataLoaderByKey(CacheKey.cates));
		controller.setAttr("tags_cateList", cateList);
	}

	/**
	 * 标签缓存
	 * 
	 * @param controller
	 */
	private static void setTagsCache(Controller controller) {
		List<Tags> tags = CacheKit.get(CacheBean.TAGS, CacheBean.TAGS, JFBlogCache.getIDataLoaderByKey(CacheKey.tags));
		controller.setAttr("tags", tags);
	}

	/**
	 * 留言缓存
	 */
	private static void setMessagesCache(Controller controller) {
		Page<Message> messages = CacheKit.get(CacheBean.MESSAGES, CacheBean.MESSAGES, JFBlogCache
				.getIDataLoaderByKey(CacheKey.messages));
		controller.setAttr("messages", messages);
	}

}
