package com.jfblog.model;

import java.util.List;

import com.jfblog.bean.Constants;
import com.jfinal.plugin.activerecord.Model;
import com.jfinal.plugin.activerecord.Page;

/**
 * 分类标签
 * 
 * @author Realfighter
 * 
 */
@SuppressWarnings("serial")
public class Tags extends Model<Tags> {

	public static final Tags dao = new Tags();

	public List<Tags> findAll() {
		return find("select * from tags order by convert(name USING gbk) COLLATE gbk_chinese_ci asc");
	}

	public Page<Tags> findTags4Page(int pagenum, int pageSize, String category) {
		String except = " from tags where 1=1";
		if (!Constants.ALL.equals(category)) {
			except += " and type = '" + category + "'";
		}
		except += " order by convert(name USING gbk) COLLATE gbk_chinese_ci asc";
		return paginate(pagenum, pageSize, "select * ", except);
	}

	public List<Tags> findCateList() {
		return find("select distinct(type) cate from tags");
	}

	public List<Tags> findByCategory(String category) {
		String sql = "select * from tags where 1=1";
		if (category != null) {
			sql += " and type='" + category + "'";
		}
		sql += " order by convert(name USING gbk) COLLATE gbk_chinese_ci asc";
		return find(sql);
	}
}
