package com.jfblog.interceptor;

import com.jfblog.bean.Constants;
import com.jfblog.model.User;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;

/**
 * 后台管理员拦截器：拦截后台请求
 * 
 * @ClassName: AdminInterceptor
 * @Description: (这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2015-2-12 下午02:39:11
 * 
 */
public class AdminInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		Integer userid = ai.getController().getSessionAttr("xx566_userid");
		// 通过userid查找是否是管理员账户
		User user = User.dao.findById(userid);
		if (user.getInt("state") == Constants.ISADMIN) {// 必须是后台管理员
			ai.invoke();
		} else {
			ai.getController().redirect("/");// 跳转到首页
		}
	}

}
