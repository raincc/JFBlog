package com.jfblog.interceptor;

import java.util.List;

import com.jfblog.utils.RequestUtil;
import com.jfinal.aop.Interceptor;
import com.jfinal.core.ActionInvocation;
import com.jfinal.plugin.activerecord.Db;
import com.jfinal.plugin.activerecord.Record;

/**
 * BlackInterceptor.
 */
public class BlackInterceptor implements Interceptor {

	public void intercept(ActionInvocation ai) {
		String ip = RequestUtil.getIpAddr(ai.getController().getRequest());
		List<Record> blacks = Db.find("select * from blacks where ip =?", ip);
		if (blacks.isEmpty()) {// 不是黑名单
			ai.invoke();
		} else {
			ai.getController().redirect("/");// 跳转到首页
		}
	}

}
