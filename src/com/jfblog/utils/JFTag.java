package com.jfblog.utils;

import javax.servlet.jsp.JspTagException;
import javax.servlet.jsp.jstl.core.ConditionalTagSupport;

public class JFTag extends ConditionalTagSupport {

	/**
	 * @Fields serialVersionUID :
	 */
	private static final long serialVersionUID = -4887684116120954077L;

	private Integer type;// 文章类型码

	private Integer code;// 所属类型码

	public void setType(Integer type) {
		this.type = type;
	}

	public void setCode(Integer code) {
		this.code = code;
	}

	@Override
	protected boolean condition() throws JspTagException {
		return (type & code) > 0;
	}

}
