package com.jfblog.validator;

import com.jfblog.model.User;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * 注册表单校验
 * 
 * @ClassName: RegValidator
 * @author Realfighter
 * @date 2015-4-11 下午12:56:52
 * 
 */
public class RegValidator extends Validator {

	@Override
	protected void handleError(Controller c) {
		c.keepPara("name");
		c.keepPara("website");
		c.render("register.jsp");
	}

	@Override
	protected void validate(Controller c) {
		String username = c.getPara("name");
		validateString("name", 1, 20, "error_info", "用户名长度为【1-20】");
		validateRequired("password", "error_info", "密码不能为空");
		validateRequired("repassword", "error_info", "密码不能为空");
		validateEqualField("password", "repassword", "error_info", "两次密码输入不一致");
		String website = c.getPara("website");
		if (!"".equals(website) && !website.startsWith("http://")) {
			addError("error_info", "网站地址必须以【http://】开头");
		}
		User user = User.dao.findByName(username);
		if (user != null) {
			addError("error_info", "用户名已存在");
		}
	}

}
