package com.jfblog.validator;

import com.jfblog.model.User;
import com.jfinal.core.Controller;
import com.jfinal.validate.Validator;

/**
 * 登录表单校验
 * 
 * @ClassName: LoginValidator
 * @author Realfighter
 * @date 2015-4-11 下午12:56:52
 * 
 */
public class LoginValidator extends Validator {

	@Override
	protected void handleError(Controller c) {
		c.keepPara("name");
		c.render("login.jsp");
	}

	@Override
	protected void validate(Controller c) {
		validateString("name", 1, 20, "error_info", "用户名长度为【1-20】");
		validateString("password", 1, 32, "error_info", "密码长度为32位");
		String username = c.getPara("name");
		String password = c.getPara("password");
		User user = User.dao.findLoginUser(username, password);
		if (user == null) {
			addError("error_info", "用户名或密码错误");
		}
	}

}
