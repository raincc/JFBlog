package com.jfblog.bean;

/**
 * 
 * @ClassName: CacheBean
 * @Description: 缓存keys(这里用一句话描述这个类的作用)
 * @author Realfighter
 * @date 2014-12-10 下午05:03:33
 * 
 */
public class CacheBean {

	public static final String ARTICLES = "articles";// 文章详情

	public static final String MESSAGES = "messages";// 最新留言

	public static final String ARCHIVE_TIMES = "archive_times";// 归档日期

	public static final String TAGS = "tags";// 分类标签

	public static final String FRIENDLYS = "friendlys";// 友情链接

	public static final String CATES = "cates";// 标签分类

}
