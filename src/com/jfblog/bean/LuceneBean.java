package com.jfblog.bean;

/**
 * lucene检索
 * 
 * @author Realfighter
 *
 */
public class LuceneBean {

	private Integer aid;// 文章ID
	private String title;// 文章标题
	private String remark;// 文章摘要

	public LuceneBean(Integer aid, String title, String remark) {
		this.aid = aid;
		this.title = title;
		this.remark = remark;
	}

	public Integer getAid() {
		return aid;
	}

	public void setAid(Integer aid) {
		this.aid = aid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getRemark() {
		return remark;
	}

	public void setRemark(String remark) {
		this.remark = remark;
	}

}
